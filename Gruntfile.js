module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    copy: {
      main: {
        files: [
          {
            flatten:true,
            expand: true,
            src: ['bower_components/font-awesome/fonts/*'],
            dest: 'public/fonts/'
          },
        ],
      },
    },

    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        dest: 'public/js/<%= pkg.name %>.min.js',
        src: [
          'bower_components/jquery/dist/jquery.min.js',
          'bower_components/jquery-form/jquery.form.js',
          'public/js/src/*.js'
        ]
      }
    },

    sass: {
			dist: {
				files: {
					'public/css/src/styles.css' : 'sass/styles.scss'
				}
			}
		},

    cssmin: {
      options: {
        shorthandCompacting: false,
        roundingPrecision: -1
      },
      target: {
        files: {
          'public/css/<%= pkg.name %>.min.css': [
            'bower_components/font-awesome/css/font-awesome.min.css',
            'public/css/src/*.css'
          ]
        }
      }
    },

		watch: {
			js: {
				files: 'public/js/src/*.js',
				tasks: ['uglify']
			},
			css: {
				files: 'sass/*.scss',
				tasks: ['sass', 'cssmin']
			}
		}

  });

  // Copy
  grunt.loadNpmTasks('grunt-contrib-copy');

  // Javascript
  grunt.loadNpmTasks('grunt-contrib-uglify');

  // CSS
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-cssmin');

  // Watch
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask('default', ['copy', 'uglify', 'sass', 'cssmin', 'watch']);

};
