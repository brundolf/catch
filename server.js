// express
var express = require('express');
var bodyParser = require('body-parser');
var multer = require('multer');
var secureRandom = require('secure-random');
var fs = require("fs");
//var mongoose = require('mongoose');

// database
/*
mongoose.connect('mongodb://localhost:27017');
var models = require('./model/File');
*/

// application
var app = express();
var oneDay = 8.64e+7;
app.use(express.static('public', { maxAge: oneDay }));

// body parser
app.use(bodyParser.urlencoded());

// uploading
var upload = multer({ dest: 'uploads/' });

// templating engine
var swig = require('swig');
swig.setDefaults({ cache: false });

app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('view cache', false);

// TEMPORARY
app.locals = {
	files: []
};

// bytes to code string
var characters = [
	'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
];
var codeFromBytes = function(bytesArray) {
	var code = '';
	for(var i = 0; i < bytesArray.length; i++) {
		var byte = bytesArray[i];
		var byteBound = byte % characters.length;
		var character = characters[byteBound];
		code += character;
	}
	return code;
};

var fileLifetimeInMinutes = 20;
var cleanOldFiles = function() {
	var fileLifetimeInMilliseconds = fileLifetimeInMinutes * 60 * 1000;

	for(var i = 0; i < app.locals.files.length; i++) {
		var file = app.locals.files[i];
		if(Date.now() - file.uploadTime > fileLifetimeInMilliseconds) {
			fs.unlink('./uploads/' + file.filename);
			app.locals.files.splice(i, 1);
			i--;
		}
	}
};

var codeInUse = function(code) {
	for(var i = 0; i < app.locals.files.length; i++) {
		var file = app.locals.files[i];
		if(file.code === code) {
			return true;
		}
	}
	return false;
};

// pages
app.get('/', function(request, response) {
	console.log('VISIT')

	cleanOldFiles();

	response.render('index.html');
});

// api
app.post('/upload', upload.single('file'), function(request, response){
	console.log('UPLOAD')

	// debugging
	var cache = [];
	var thing = JSON.stringify(request.file, function(key, value) {
			if (typeof value === 'object' && value !== null) {
					if (cache.indexOf(value) !== -1) {
							// Circular reference found, discard key
							return 'CIRCULAR';
					}
					// Store value in our collection
					cache.push(value);
			}
			return value;
	});

	cleanOldFiles();

	var code;
	do {
		code = codeFromBytes(secureRandom(6, { type: 'Array' }));
	} while( codeInUse(code) );
	app.locals.files.push(
		{
			code: code,
			filename: request.file.filename,
			originalName: request.file.originalname,
			uploadTime: Date.now()
		}
	);
	response.send(code);
});
app.get('/download/:code', function(request, response){
	console.log('DOWNLOAD')

	cleanOldFiles();

	for(var i = 0; i < app.locals.files.length; i++) {
		var file = app.locals.files[i];

		if(file.code === request.params.code.toUpperCase()) {
			response.download(__dirname + '/uploads/' + file.filename, file.originalName);
			return;
		}
	}
	response.sendStatus(404);
});


// start
app.set('port', (process.env.PORT || 8000));
app.listen(app.get('port'));
console.log('Listening on port ' + app.get('port') + '...');
