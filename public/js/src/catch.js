if(window.location.hostname.indexOf('ancient-scrubland-79869') !== -1) {
	window.location = 'http://catch.brandonsmith.ninja';
}

$(document).ready(function(){

	var getHash = function() {
		var hash = window.location.hash;
		hash = hash.replace('#', '');
		if(hash.indexOf('?') > -1) {
			hash = hash.substring(0, hash.indexOf('?'))
		}
		return hash;
	}
	var getQueryParams = function() {
		var queryString = window.location.hash;

		if(queryString.indexOf('?') < 0) {
			return { };
		} else {
			queryString = queryString.substring(queryString.indexOf('?') + 1);
			var params = queryString.split('&');

			var paramsObject = {};
			for(var i = 0; i < params.length; i++) {
				paramsObject[ params[i].split('=')[0] ] = params[i].split('=')[1];
			}
			return paramsObject;
		}
	}

	var openAction = function(action) {
		$('.action').each(function(){
			if(!$(this).is(action)) {
				$(this).removeClass('selected');
				$(this).addClass('hidden');
				$(this).find('.expanded-content').removeClass('selected');
			}
		});

		$(action).addClass('selected');
		$(action).removeClass('hidden');
		setTimeout(function(){
			$(action).find('.expanded-content').addClass('selected');
		}, 300)

		$('.back-button').removeClass('hidden');
	};
	var closeActions = function() {
		$('.action').removeClass('hidden').removeClass('selected');
		$('.action .expanded-content').removeClass('selected');
		$('.back-button').addClass('hidden');
	};

	// click on action
	$('.action').click(function(){
		openAction($(this));
	});

	$('.back-button').click(function(){
		closeActions();
		window.location.hash = '';
	});
	$('.hero h1').click(function(){
		closeActions();
		window.location.hash = '';
	});

	// upload file
	$('input[name="file"]').change(function() {
		$(this).closest('form').ajaxForm({
			success: function(response) {
				$('.message').addClass('hidden');
				setTimeout(function(){
					$('.message').css('display', 'none');
					$('.upload-form').css('display', 'none');
					$('.upload-result').removeClass('hidden');
					$('.code').val(response).select();
					var downloadLink = window.location.origin + '#download?code=' + response;
					$('a.download-link').text(downloadLink).attr('href', downloadLink);
				}, 300)
			}
		}).submit();
	});

	// enter code
	var downloadFile = function(code) {
		$.get({
			url: '/download/' + code,
			success: function() {
				window.location = '/download/' + code;
			},
			error: function() {
				$('.error-message').removeClass('hidden');
			}
		});
	}
	$('input.code-input').keypress(function(event) {
		$('.error-message').addClass('hidden');

		var field = $(this);
		if(event.keyCode === 13) {
			downloadFile($(field).val());
		}
	})
	$('.download-button').click(function(event) {
		var field = $('input.code-input');
		downloadFile($(field).val());
	})

	var respondToHash = function() {
		if(getHash() === 'upload') {
			openAction($('.upload-action'));
		} else if(getHash() === 'download') {
			openAction($('.download-action'));
			if(getQueryParams().code) {
				$('input.code-input').val(getQueryParams().code);
			}
		} else {
			closeActions();
		}
	}
	window.onhashchange = respondToHash;
	respondToHash();

	$('.upload-action').click(function(){
		window.location.hash = '#upload';
	});
	$('.download-action').click(function(){
		window.location.hash = '#download';
	});

});
